#!/usr/bin/env python3

from random import seed
from random import randint
from timeit import default_timer as timer

from BinarySearchTree import BinarySearchTree
from AVLTree1 import AVLTree as AVL1

struct = [BinarySearchTree(), AVL1()]

#size = 100000
#size = 50000
size = 20000
r = 2*32
iterations = 10000

items = []
for _ in  range(size):
	items += [randint(1,r)]

for bst in struct:
	start = timer()
	for i in items:
		bst.insert(i, i)
	end = timer()

	print("Time to populate "+type(i).__name__+":"+str(end-start))
