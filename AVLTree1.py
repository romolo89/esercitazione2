from BinarySearchTree import BSTNode, BinarySearchTree

debug = 0

class AVLNode(BSTNode):
    pass


class AVLTree(BinarySearchTree):
    def __init__(self):
        super(AVLTree, self).__init__()


    def insert(self, key, data):
        if debug:
            print("Insert\t"+str(key))
        super(AVLTree, self).insert(key, data)
        

    def delete(self, key):
        if debug:
            print("delete\t"+str(key))
        super(AVLTree, self).delete(key)

