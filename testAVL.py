#!/usr/bin/env python3

from AVLTree1 import AVLTree

mybst = AVLTree()
mybst.insert(20, "payload")
print(mybst)

mybst.insert(25, "payload")
print(mybst)
mybst.insert(22, "payload")
print(mybst)
mybst.insert(18, "payload")
print(mybst)
mybst.insert(17, "payload")
print(mybst)
mybst.insert(100, "payload")
print(mybst)
mybst.insert(105, "payload")
print(mybst)
mybst.insert(26, "payload")
print(mybst)
mybst.insert(140, "payload")
print(mybst)
mybst.insert(18, "payload")
print(mybst)
mybst.insert(19, "payload")
print(mybst)
mybst.insert(16, "payload")
print(mybst)
mybst.insert(50, "payload")
print(mybst)
mybst.insert(45, "payload")
print(mybst)
mybst.insert(47, "payload")
print(mybst)


print("NUM NODES:"+str(len(mybst)))
print("NUM ARITY:"+str(mybst.arity()))


mybst.delete(22)
print(mybst)
mybst.delete(18)
print(mybst)
mybst.delete(25)
print(mybst)
mybst.delete(26)
print(mybst)
