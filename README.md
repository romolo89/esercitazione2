# Informazioni sul contenuto della cartella

Questa cartella contiene una gerarchia di alberi binari.

In particolare:
 - Tree.py definisce l'interfaccia di un tipo di dato astratto albero
 - BinaryTree.py implementa un albero binario
 - BinarySearchTree.py implementa un albero binario di ricerca
 - AVLTree.py implementa un albero auto-bilanciato AVL

L'implementazione di AVLTree è quella che deve essere completata per questa esercitazione.
